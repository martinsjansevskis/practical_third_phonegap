/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    onDeviceReady: function() {
        this.receivedEvent();
        app.mediaRecorder = null;
    },

    // Update DOM on a Received Event
    receivedEvent: function() {
        var scope = this;
        navigator.mediaDevices.getUserMedia({
            'audio': true,
            'video': {
                facingMode: 'environment'
            }
        }).then(function(mediaStream) {
            console.log('push this there');
            // Show camera screen
            scope.mediaStream = mediaStream;
            scope.mediaControl = document.getElementById("vid");
            scope.mediaControl.srcObject = mediaStream;
            scope.mediaControl.src = URL.createObjectURL(mediaStream);
        });

        scope.startRecording = document.getElementById("startRecording");
        scope.startRecording.addEventListener("click", function(){
            scope.startRecord(scope);
        });
        scope.stopRecording = document.getElementById("stopRecording");
        scope.stopRecording.disabled = true;
        scope.stopRecording.addEventListener("click", function(){
            scope.stopRecord(scope);
        });
    },

    // Start recording
    startRecord: function(scope) {
        scope.stopRecording.disabled = false;
        scope.startRecording.disabled = true;
        scope.mediaRecorder = new MediaRecorder(scope.mediaStream);
        scope.mediaRecorder.onstart = function() {
            console.log('Recording Started');
        };
        scope.mediaRecorder.start();
    },

    // Stop recording
    stopRecord: function(scope) {
        scope.stopRecording.disabled = true;
        scope.startRecording.disabled = false;
        scope.mediaRecorder.ondataavailable = function(blob) {
            console.log('data available for saving');
            var playbackTag = document.getElementById("playback");
            if(device.platform === 'iOS') {            // iOS device
                playbackTag.src = mediaRecorder.src;
            } else {                                     // Android device
                var recordedChunks = [];
                recordedChunks.push(blob.data);
                playbackTag.src = URL.createObjectURL(new Blob(recordedChunks));
            }
        }

        scope.mediaRecorder.onstop = function() {
            console.log('Recording Stopped');
        };
        scope.mediaRecorder.stop();
    }
};

app.initialize();
